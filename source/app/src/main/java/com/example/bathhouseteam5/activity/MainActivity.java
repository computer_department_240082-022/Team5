package com.example.bathhouseteam5.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bathhouseteam5.R;
import com.example.bathhouseteam5.adapter.MyAdapter;
import com.example.bathhouseteam5.bean.BathBean;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private MyAdapter myAdapter;

    List<BathBean> bathList;
    int maxSize = 1010;
    int bathSum = 1010;
    String[] bathName = {"金海豪洗浴中心", "御尚洗浴中心", "凤凰城洗浴中心", "馨泉洗浴中心", "金湾国际洗浴中心", "悦尚洗浴中心"};
    String[] bathPrice = {"10", "10", "11", "12", "9", "8"};
    String[] bathDistance = {"700", "800", "1200", "1700", "2100", "3000"};
    String[] bathAddress = {"中山路15号", "振兴路116号", "福强路93号", "八达路36号", "安德路127号", "百花路77号"};
    int[] bathCapcity = {70, 60, 43, 25, 79, 80};
    int[] bathNumber = {66, 63, 40, 40, 80, 60};
    int i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        bathList = getData();
        myAdapter = new MyAdapter(this);
        myAdapter.setList(bathList);
        recyclerView.setAdapter(myAdapter);
    }


    private List<BathBean> getData() {
        List<BathBean> list = new ArrayList<BathBean>();
        //数据源在这里设置
        for (i = 0; i < bathSum && i < bathName.length; i++) {
            BathBean bathBean;
            bathBean = new BathBean();
            bathBean.setImgid(R.drawable.f1);
            bathBean.setName(bathName[i]);
            bathBean.setPrice(bathPrice[i]);
            bathBean.setDistance(bathDistance[i]);
            bathBean.setAddress(bathAddress[i]);
            if (bathCapcity[i] > bathNumber[i]) {
                bathBean.setBathEmptyNum(bathCapcity[i] - bathNumber[i]);
                bathBean.setBathLineNum(0);
            } else {
                bathBean.setBathEmptyNum(0);
                bathBean.setBathLineNum(bathNumber[i] - bathCapcity[i]);
            }

            list.add(bathBean);
        }
        return list;
    }
}
