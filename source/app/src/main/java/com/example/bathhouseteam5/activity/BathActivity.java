package com.example.bathhouseteam5.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bathhouseteam5.R;


public class BathActivity extends AppCompatActivity {

    private TextView name, price, distance, address, empty, line;
    private ImageView imageView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bathactivity_218wy);
        initView();

        int pictrue = getIntent().getIntExtra("pic", 0);
        String name1 = getIntent().getStringExtra("name");
        String price1 = getIntent().getStringExtra("price");
        String distance1 = getIntent().getStringExtra("dis");
        String address1 = getIntent().getStringExtra("address");
        int empty1 = getIntent().getIntExtra("empty", 0);
        int line1 = getIntent().getIntExtra("line", 0);

        imageView2.setImageResource((int) pictrue);
        name.setText("澡堂名称:" + name1);
        price.setText("价格:" + price1);
        distance.setText("距离：" + distance1 + "米");
        address.setText("地址：" + address1);
        empty.setText("空位数：" + empty1 + "人");
        line.setText("排队人数：" + line1 + "人");

    }

    private void initView() {
        imageView2 = findViewById(R.id.imageView3);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        distance = findViewById(R.id.distance);
        address = findViewById(R.id.address);
        empty = findViewById(R.id.empty);
        line = findViewById(R.id.linenum);
    }
}
