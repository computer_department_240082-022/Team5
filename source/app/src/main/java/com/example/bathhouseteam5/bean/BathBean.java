package com.example.bathhouseteam5.bean;

public class BathBean {
    private String name;
    private int imgid;
    private String price;
    private String distance;
    private String address;
    private int bathEmptyNum;
    private int bathLineNum;

    public int getBathEmptyNum() {
        return bathEmptyNum;
    }

    public void setBathEmptyNum(int bathEmptyNum) {
        this.bathEmptyNum = bathEmptyNum;
    }

    public int getBathLineNum() {
        return bathLineNum;
    }

    public void setBathLineNum(int bathLineNum) {
        this.bathLineNum = bathLineNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgid() {
        return imgid;
    }

    public void setImgid(int imgid) {
        this.imgid = imgid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
