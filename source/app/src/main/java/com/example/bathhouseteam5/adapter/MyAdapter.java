package com.example.bathhouseteam5.adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bathhouseteam5.R;
import com.example.bathhouseteam5.activity.BathActivity;
import com.example.bathhouseteam5.bean.BathBean;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BathBean> list;
    private Context context;

    public MyAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<BathBean> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final BathBean bathBean = list.get(position);
        if (bathBean == null)
            return;
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.imageView.setImageResource(bathBean.getImgid());
        viewHolder.name.setText(bathBean.getName());
        viewHolder.price.setText(bathBean.getPrice() + "元/次");
        viewHolder.distance.setText("距您" + bathBean.getDistance() + "米");


        //条目的响应事件,Toast显示当前条目详细信息即introduction
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BathActivity.class);
                intent.putExtra("pic", bathBean.getImgid());
                intent.putExtra("name", bathBean.getName());
                intent.putExtra("price", bathBean.getPrice());
                intent.putExtra("dis", bathBean.getDistance());
                intent.putExtra("address", bathBean.getAddress());
                intent.putExtra("empty", bathBean.getBathEmptyNum());
                intent.putExtra("line", bathBean.getBathLineNum());

                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView name, price, distance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            distance = (TextView) itemView.findViewById(R.id.distance);
        }
    }
}
